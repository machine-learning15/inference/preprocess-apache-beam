#!/usr/bin/env bash

runner=$1
now=$(date -u +"%Y-%m-%d-%H-%M-%S")
job_name="preprocess-birthdata-parquet-${now}"
echo "Executing ${runner} job=${job_name}..."

if [ "$runner" == "DirectRunner" ]; then
  echo "executing Direct Runner"
  python write_to_parquet.py \
  --runner "${runner}" \
  --dataset "${DATASET}" \
  --output "${OUTPUT}" \
  --project "${PROJECT}" \
  --region "${REGION}" \
  --staging_location "${STAGING_LOC}" \
  --temp_location "${TEMP_LOC}" \
  --job_name "${job_name}" \
  --debug
else
  echo "executing Dataflow Runner"
  python write_to_parquet.py \
  --runner "${runner}" \
  --dataset "${DATASET}" \
  --output "${OUTPUT}" \
  --project "${PROJECT}" \
  --region "${REGION}" \
  --staging_location "${STAGING_LOC}" \
  --temp_location "${TEMP_LOC}" \
  --job_name "${job_name}"
fi

echo "Finished executing job=${job_name}..."
