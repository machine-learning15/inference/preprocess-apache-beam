from __future__ import absolute_import

import argparse
import logging
import os
import apache_beam as beam
import pyarrow


def get_query(dataset, step='train', debug=True):
    """Use 2016 and 2017 data for training, 2018 data for test, and random sample data for debug"""
    query = """
    SELECT 
        CAST(MAGER AS INT64) AS M_AGE,
        CASE
            WHEN MBRACE = '1' THEN 'White'
            WHEN MBRACE = '2' THEN 'Black'
            WHEN MBRACE = '3' THEN 'NativeAmerican'
            WHEN MBRACE = '4' THEN 'Asian'
        END AS M_RACE,
        CASE 
            WHEN DMAR = '1' THEN 'Y'
            WHEN DMAR = '2' THEN 'N'
        END AS MARRIED,
        CAST(MEDUC AS INT64) AS M_EDU,
        CAST(FEDUC AS INT64) AS F_EDU,
        CASE             
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 11 AND 15 THEN '11-15'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 15 AND 19 THEN '15-19'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 20 AND 24 THEN '20-24'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 25 AND 29 THEN '25-29'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 30 AND 34 THEN '30-34'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 35 AND 39 THEN '35-39'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 40 AND 44 THEN '40-44'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 45 AND 49 THEN '45-49'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 50 AND 54 THEN '50-54'
            WHEN CAST(FAGECOMB AS INT64) BETWEEN 55 AND 98 THEN '55-98'
            ELSE 'Unknown'            
        END AS F_AGE,       
        CASE
            WHEN PRECARE5 = '1' THEN '1-3M'
            WHEN PRECARE5 = '2' THEN '4-6M'
            WHEN PRECARE5 = '3' THEN '7-FinalM'
            WHEN PRECARE5 = '4' THEN 'NoPrenatalCare'
            WHEN PRECARE5 = '5' THEN 'Unknown'
        END AS PRECARE_M,
        CIG_REC AS CIG_USE,
        CAST(DPLURAL AS INT64) AS PLURALITY,
        SEX,
        CASE
            WHEN GESTREC3 = '1' THEN '<37 W'
            WHEN GESTREC3 = '2' THEN '>=37 W'
        END AS GEST_W,
        CASE
            WHEN BWTR4 = '1' THEN '< 1500 gr'
            WHEN BWTR4 = '2' THEN '< 2500 gr'
            WHEN BWTR4 = '3' THEN '>= 2500 gr'
        END AS BIRTH_WEIGHT,
        CASE 
            WHEN MBSTATE_REC = '1' THEN 'USA'
            WHEN MBSTATE_REC = '2' THEN 'Not USA'
            WHEN MBSTATE_REC = '3' THEN 'Unknown'
        END AS NATIVE,
        CASE 
            WHEN RESTATUS = '1' THEN 'Resident'
            WHEN RESTATUS = '2' THEN 'IntraState'
            WHEN RESTATUS = '3' THEN 'InterState'
            WHEN RESTATUS = '4' THEN 'Foreign'
        END as RESIDENCE,
        CAST(PREVIS_REC AS INT64) AS PREVISIT,
        MAR_P AS PATERNITY_ACK,
        NO_RISKS AS NO_RISK,
        NO_INFEC AS NO_INFC,
        NO_MMORB
    FROM `{dataset}`
    WHERE 
        MAGER IS NOT NULL AND
        MBRACE IS NOT NULL AND
        DMAR IS NOT NULL AND
        MEDUC IS NOT NULL AND MEDUC <> '9' AND
        FEDUC IS NOT NULL AND
        FAGECOMB IS NOT NULL AND
        PRECARE5 IS NOT NULL AND
        CIG_REC IS NOT NULL AND CIG_REC <> 'U' AND
        DPLURAL IS NOT NULL AND
        SEX IS NOT NULL AND
        GESTREC3 IS NOT NULL AND GESTREC3 <> '3' AND
        BWTR4 IS NOT NULL AND BWTR4 <> '4' AND
        MBSTATE_REC IS NOT NULL AND
        RESTATUS IS NOT NULL AND
        PREVIS_REC IS NOT NULL AND
        MAR_P IS NOT NULL AND
        NO_RISKS IS NOT NULL AND NO_RISKS <> '9' AND
        NO_INFEC IS NOT NULL AND NO_INFEC <> '9' AND
        NO_MMORB IS NOT NULL AND NO_MMORB <> '9' AND
        DOB_YY IN {timerange}
"""
    result_query = query.format(dataset=dataset,
                                timerange="('2016', '2017')" if step == "train" else "('2018')")
    if debug:
        result_query += " AND RAND() < 0.008"

    return result_query


def get_schema():
    """Construct pyarrow parquet schema"""
    return pyarrow.schema([
        ('M_AGE', pyarrow.int64()),
        ('M_RACE', pyarrow.string()),
        ('MARRIED', pyarrow.string()),
        ('M_EDU', pyarrow.int64()),
        ('F_EDU', pyarrow.int64()),
        ('F_AGE', pyarrow.string()),
        ('PRECARE_M', pyarrow.string()),
        ('CIG_USE', pyarrow.string()),
        ('PLURALITY', pyarrow.int64()),
        ('SEX', pyarrow.string()),
        ('GEST_W', pyarrow.string()),
        ('BIRTH_WEIGHT', pyarrow.string()),
        ('NATIVE', pyarrow.string()),
        ('RESIDENCE', pyarrow.string()),
        ('PREVISIT', pyarrow.int64()),
        ('PATERNITY_ACK', pyarrow.string()),
        ('NO_RISK', pyarrow.string()),
        ('NO_INFC', pyarrow.string()),
        ('NO_MMORB', pyarrow.string())
    ])


def read_from_bq(pipeline, dataset, step, debug):
    query = get_query(dataset, step, debug)
    raw_data = pipeline | '{} - Read Data From BigQuery'.format(step) >> beam.io.Read(
        beam.io.BigQuerySource(query=query, use_standard_sql=True))

    return raw_data


def write_parquet(records, location, step):
    _ = records | '{} - Write Parquet'.format(step) >> beam.io.parquetio.WriteToParquet(
        file_path_prefix=os.path.join(location, '{}'.format(step)),
        file_name_suffix='.parquet',
        schema=get_schema())


def run_pipeline(custom_args, beam_args):
    """Defines and runs the pipeline with given args"""

    with beam.Pipeline(argv=beam_args) as pipeline:
        # get and write training data
        train_data = read_from_bq(pipeline, custom_args.dataset, 'train', custom_args.debug)

        # get and write test data
        test_data = read_from_bq(pipeline, custom_args.dataset, 'test', custom_args.debug)

        # write train data to target bucket with prefix
        write_parquet(train_data, custom_args.output, 'train')

        # write test data to target bucket with prefix
        write_parquet(test_data, custom_args.output, 'test')


if __name__ == '__main__':
    # Retrieve args
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--debug',
        action="store_true",
        default=False,
        dest='debug',
        help="Uses small dataset")
    parser.add_argument(
        '--dataset',
        dest='dataset',
        help='BigQuery dataset name to retrieve data from')
    parser.add_argument(
        '--output',
        dest='output',
        help='Where the parquet files will be saved')

    custom_args, beam_args = parser.parse_known_args()

    # Clean up the output if exists
    if beam.io.filesystems.FileSystems.exists(custom_args.output):
        beam.io.filesystems.FileSystems.delete([custom_args.output])

    # Set logging level
    logging.getLogger().setLevel(logging.INFO)

    # Run pipeline
    run_pipeline(custom_args, beam_args)
