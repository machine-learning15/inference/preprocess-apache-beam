# preprocess-apache-beam
[![pipeline status](https://gitlab.com/machine-learning15/inference/preprocess-apache-beam/badges/development/pipeline.svg)](https://gitlab.com/machine-learning15/inference/preprocess-apache-beam/-/commits/development)


[<img src="arch.png" width="700" height="450"/>](arch.png)  
Retrieves filtered cdc-birth data from BigQuery, filters and pre-processes data, then writes it to Google Cloud storage with Apache Beam in parquet format.  
- DirectRunner is used to test with small data and runs locally in the docker container.
- DataFlowRunner is used with full data, the job is run on Dataflow service.
- Refer [here]() for the upstream job that retrieves data from CDC's ftp servers, and then writes to BigQuery in batch.

## CI/CD
- Builds the docker image, then pushes it to Gitlab registry and GCR. 
- Deploys GCR image to GKE, sets up the runner type for the Apache-Beam job. If debug flag is enabled, then the job will retrieve 8% of data from BigQuery.
  - Uses DirectRunner if it is a test run, and debug flag is enabled. Then the pipeline will run locally in the docker container, and sink will still be GCS.
  - Uses DataflowRunner if it is a production run, then the job will run in remote DataFlow service, and sink will be GCS.
    
## Local Development
### Docker
```bash
# setup local gcloud docker login
gcloud auth configure-docker

# build/test/run docker img
cp "path/to/service-account.json" deployment/
cd deployment
docker build -t beam-test .
docker run -it --rm -e project="project-id" -e output="gs://bucket/output" -e dataset="BiqQuery.dataset" \
-e region="region" -e staging_location="gs://bucket/staging" -e temp_location="gs://bucket/temp" \
-e GOOGLE_APPLICATION_CREDENTIALS="service-account.json" beam-test

# push to registry
docker build -t us.gcr.io/project-id/beam-test .
docker push us.gcr.io/project-id/beam-test
```
### K8s
```bash
# update kubectl config context to this GKE cluster
gcloud container clusters get-credentials cluster-name --zone cluster-zone --project project-id

# check/delete scrts
kubectl get secret app-secret -o jsonpath='{.data}'
echo 'base64-encoded-text' | base64 --decode
kubectl delete secret app-secret

# deploy to remote cluster from local
kubectl apply -f job.yaml
kubectl describe jobs/preprocess-birthdata-beam
kubectl get pods
```
### Permission checks
https://cloud.google.com/recommender



